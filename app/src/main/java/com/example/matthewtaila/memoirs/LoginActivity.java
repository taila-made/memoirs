package com.example.matthewtaila.memoirs;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.matthewtaila.memoirs.Constants.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "LoginActivity";
    
    //  - UI
    RelativeLayout blueBackground;
    RelativeLayout rl_signInContainer;
    TextView tv_welcomeUser;
    TextView tv_email;
    TextView tv_password;
    EditText et_emailField;
    EditText et_passwordField;
    Button btn_signUp;
    Button btn_signIn;

    // - Logic
    boolean animateSignIn = false;
    
    // - Firebase.Authentication
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        referenceViews(); // reference views
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth = FirebaseAuth.getInstance();
        setupAuthStateListener(); //
        firebaseAuth.addAuthStateListener(authListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (firebaseAuth != null){
            firebaseAuth.removeAuthStateListener(authListener);
        }
    }

    private void setupAuthStateListener() {
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    Log.i(TAG, "onAuthStateChanged: User is signed in");
                    if(!animateSignIn) {
                        startAlbumListActivity();
                    }
                } else {
                    Log.i(TAG, "onAuthStateChanged: User is NOT signed in");
                }
            }


        };
    }

    private void startAlbumListActivity(){
        Intent intent = new Intent(this, AlbumListActivity.class);
        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, blueBackground, "transition_blue_bg");
        startActivity(intent, optionsCompat.toBundle());
        supportFinishAfterTransition();
    }

    private void referenceViews() {
        blueBackground = (RelativeLayout)findViewById(R.id.blueBackground);
        rl_signInContainer = (RelativeLayout) findViewById(R.id.signInContainer);
        tv_welcomeUser= (TextView)findViewById(R.id.tv_welcomeUser);
        tv_email = (TextView) findViewById(R.id.tv_password);
        tv_password = (TextView) findViewById(R.id.tv_password);
        et_emailField = (EditText) findViewById(R.id.et_email);
        et_passwordField = (EditText) findViewById(R.id.et_password);
        btn_signIn = (Button) findViewById(R.id.btn_signIn);
        btn_signUp = (Button) findViewById(R.id.btn_signUp);

        // - SetViews off screen
        tv_welcomeUser.setTranslationX(-Utils.getScreenWidth(this));
        rl_signInContainer.setTranslationY(-Utils.getScreenHeight(this));
        tv_email.setTranslationY(Utils.getScreenHeight(this));
        tv_password.setTranslationY(Utils.getScreenHeight(this));
        et_emailField.setTranslationY(Utils.getScreenHeight(this));
        et_passwordField.setTranslationY(Utils.getScreenHeight(this));
        btn_signIn.setTranslationY(Utils.getScreenHeight(this));
        btn_signUp.setTranslationY(Utils.getScreenHeight(this));

        // - onClickListener
        btn_signIn.setOnClickListener(this);
        btn_signUp.setOnClickListener(this);

        animateSignUpViews();
    }

    private void animateSignUpViews() {

        rl_signInContainer.animate()
                .setDuration(300)
                .setStartDelay(100)
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(1));

        tv_email.animate()
                .setDuration(300)
                .setStartDelay(200)
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(1));

        et_emailField.animate()
                .setDuration(300)
                .setStartDelay(200)
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(1));

        tv_password.animate()
                .setDuration(300)
                .setStartDelay(300)
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(1));

        et_passwordField.animate()
                .setDuration(300)
                .setStartDelay(300)
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(1));

        btn_signIn.animate()
                .setDuration(300)
                .setStartDelay(400)
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(1));

        btn_signUp.animate()
                .setDuration(300)
                .setStartDelay(500)
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(1));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_signIn:
                animateSignIn = true;
                String signInEmail = et_emailField.getText().toString();
                String signInPassword = et_passwordField.getText().toString();
                onSignIn(signInEmail, signInPassword);
                break;
            case R.id.btn_signUp:
                String createUserEmail = et_emailField.getText().toString();
                String createUserPassword = et_passwordField.getText().toString();
                createUser(createUserEmail, createUserPassword);
                break;
        }
    }

    private void createUser(final String email, String password) {
        if (!validateSignInAndSignUpForm()){return;}
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Log.i(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());
                if (!task.isSuccessful()) {
                    if(task.getException() != null) {
                        Log.i(TAG, "Create user failed with error: " + task.getException().getMessage());
                    }
                    Toast.makeText(LoginActivity.this, "FAILED", Toast.LENGTH_SHORT).show();
                } else { // Was succesful !
                    Log.i(TAG, "Successfully created user with email: " + email);
                    signInAnimation();
                }
            }
        });
    }

    private void signInAnimation() {
        btn_signIn.setVisibility(View.INVISIBLE);
        btn_signUp.setVisibility(View.INVISIBLE);

        int centerX = btn_signIn.getLeft()+btn_signIn.getMeasuredWidth()/2;
        int centerY = btn_signIn.getTop()+btn_signIn.getMeasuredWidth()/2;
        Animator animator = Utils.circleReveal(blueBackground, centerX, centerY, Math.max(blueBackground.getMeasuredHeight(), blueBackground.getMeasuredWidth()));
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                tv_welcomeUser.animate()
                        .translationX(0)
                        .setDuration(300)
                        .setStartDelay(300)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                startAlbumListActivity();
                            }
                        })
                        .setInterpolator(new DecelerateInterpolator());
            }
        });
        animator.start();
    }

    private void onSignIn (final String email, String password){
        if (!validateSignInAndSignUpForm()){return;}

        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Log.i(TAG, "SIGN IN = " + task.isSuccessful());
                if (!task.isSuccessful()){
                    if(task.getException() != null) {
                        Log.i(TAG, "Sign in: failed with error: " + task.getException().getMessage());
                        animateSignIn = false;
                    }
                    Toast.makeText(LoginActivity.this, "FAILED", Toast.LENGTH_SHORT).show();
                } else {
                    signInAnimation();
                    Log.i(TAG, "Sign in: success!");
                }
            }
        });
    }

    private boolean validateSignInAndSignUpForm(){
        boolean valid = true;

        String emailEntered = et_emailField.getText().toString();
        if (TextUtils.isEmpty(emailEntered)){
            et_emailField.setError("Required");
            Toast.makeText(LoginActivity.this, "Need to enter a valid email address", Toast.LENGTH_LONG).show();
            valid = false;
        } else  {
            et_emailField.setError(null);
        }

        String passwordEntered = et_passwordField.getText().toString();
        if (TextUtils.isEmpty(passwordEntered)){
            et_passwordField.setError("Required");
            Toast.makeText(LoginActivity.this, "Need to enter a valid password", Toast.LENGTH_LONG).show();
            valid = false;
        } else  {
            et_passwordField.setError(null);
        }
        return valid;
    }
}
