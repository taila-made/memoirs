package com.example.matthewtaila.memoirs;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.Transition;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.matthewtaila.memoirs.Constants.Constantss;
import com.example.matthewtaila.memoirs.Constants.Utils;
import com.example.matthewtaila.memoirs.Model.Album;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class CreateAlbumActivity extends AppCompatActivity implements View.OnClickListener {

    // Constants
    private static final String TAG = "CreateAlbumActivity";
    public final static int PICK_PHOTO_CODE = 1046;

    // - Logic
    private Bitmap imageBitmap;

    //  - UI
    ImageView iv_albumCoverPic;
    EditText et_enteredAlbumTitle;
    Button btn_storeAlbum;
    RelativeLayout rl_container;

    private Transition.TransitionListener enterTransitionListener;


    // Firebase.RealtimeDatabase
    DatabaseReference memoirsRef;

    // - Firebase.Authentication
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authListener;

    // Firebase.Storage
    StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_album);
        referenceViews();
        onEnterTransition();
        firebaseAuth = FirebaseAuth.getInstance();
        startAuthStateListener();
    }

    private void onEnterTransition() {
        enterTransitionListener = new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(Transition transition) {

                Log.d(TAG, "onTransitionEnd: ");
                AnimatorListenerAdapter animatorListenerAdapter = new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationCancel(Animator animation) {
                        super.onAnimationCancel(animation);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        getWindow().getEnterTransition().removeListener(enterTransitionListener);
                        animateViews();
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }
                };
                int centerX = getIntent().getIntExtra("centerX", 0);
                int centerY = getIntent().getIntExtra("centerY", 0);
                Animator animator = Utils.circleReveal(rl_container, centerX, centerY, Math.max(rl_container.getMeasuredHeight(), rl_container.getMeasuredWidth()));
                animator.addListener(animatorListenerAdapter);
                animator.start();
            }

            @Override
            public void onTransitionEnd(Transition transition) {

            }

            @Override
            public void onTransitionCancel(Transition transition) {

            }

            @Override
            public void onTransitionPause(Transition transition) {

            }

            @Override
            public void onTransitionResume(Transition transition) {

            }
        };
        getWindow().getEnterTransition().addListener(enterTransitionListener);
    }

    private void animateViews() {
        iv_albumCoverPic.animate()
                .translationY(0)
                .setDuration(300)
                .setStartDelay(100)
                .setInterpolator(new AccelerateInterpolator(1));

        et_enteredAlbumTitle.animate()
                .translationY(0)
                .setDuration(300)
                .setStartDelay(200)
                .setInterpolator(new AccelerateInterpolator(1));

        btn_storeAlbum.animate()
                .translationY(0)
                .setDuration(300)
                .setStartDelay(300)
                .setInterpolator(new AccelerateInterpolator(1));
    }

    private void referenceViews() {
        // - Reference Views
        et_enteredAlbumTitle = (EditText) findViewById(R.id.et_createAlbumTitle);
        btn_storeAlbum = (Button) findViewById(R.id.btn_storeAlbum);
        iv_albumCoverPic = (ImageView) findViewById(R.id.iv_albumCoverPicture);
        rl_container = (RelativeLayout) findViewById(R.id.rl_container);

        // - Set Views off screen
        iv_albumCoverPic.setTranslationY(-Utils.getScreenHeight(this));
        et_enteredAlbumTitle.setTranslationY(Utils.getScreenHeight(this));
        btn_storeAlbum.setTranslationY(Utils.getScreenHeight(this));

        // - set onClickListener
        iv_albumCoverPic.setOnClickListener(this);
        iv_albumCoverPic.setColorFilter(ContextCompat.getColor(this, R.color.albumList));
        btn_storeAlbum.setOnClickListener(this);
    }

    private void startAuthStateListener() {
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    Log.i(TAG, "User is signed in as: " + firebaseAuth.getCurrentUser().getEmail());
                } else {
                    Log.i(TAG, "User is NOT signed in");
                }
            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_storeAlbum:
                storeAlbum();
                break;
            case R.id.iv_albumCoverPicture:
                onSelectImage();
        }
    }

    private void onSelectImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (intent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(intent, PICK_PHOTO_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == PICK_PHOTO_CODE) {
            if (data != null) {
                Uri selectedImage = data.getData();
                try {
                    imageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    iv_albumCoverPic.setImageBitmap(imageBitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void storeAlbum() {

        String albumTitle = et_enteredAlbumTitle.getText().toString();
        final Album album = new Album();
        album.setTitle(albumTitle);
        final DatabaseReference albumRef = Constantss.FB_USER_ALBUM_REF.child(firebaseAuth.getCurrentUser().getUid()).push();
        album.setUid(albumRef.getKey());

        // - Create album profile image on Firebase Storage path - Memoirs/Album Profile Pic/ User ID/ album ID
        storageReference = FirebaseStorage.getInstance().getReference()
                .child("Album profile image")
                .child(firebaseAuth.getCurrentUser().getUid())
                .child(album.getUid());

        // Process image
        iv_albumCoverPic.setDrawingCacheEnabled(true);
        iv_albumCoverPic.buildDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        // - Save image to path created
        UploadTask uploadTask = storageReference.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("FAILED", "FAILED");
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUri = taskSnapshot.getDownloadUrl();
                // - save Album object (with profile Image URL) on Firebase Realtime database - Memoirs/Albums/UserID/Album ID / SET VALUE
                album.setAlbumProfilePicURL(taskSnapshot.getDownloadUrl().toString());
                albumRef.setValue(album);
                finish();
            }
        });
    }

}
