package com.example.matthewtaila.memoirs;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "MainActivity";

    // - UI
    Button logInActivity;
    Button albumListActivity;
    Button pictureListActivity;
    Button createAlbumActivity;
    Button pictureActivity;
    Button editAlbumActivity;
    Button editPictureActivity;
    Button btnSignout;

    // - Firebase.Authentication
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        referenceViews();

        firebaseAuth = FirebaseAuth.getInstance();
        startAuthStateListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (firebaseAuth != null){
            firebaseAuth.removeAuthStateListener(authListener);
        }
    }

    private void referenceViews() {
        // - Navigation buttons
        logInActivity = (Button)findViewById(R.id.btn_loginActivity);
        albumListActivity = (Button)findViewById(R.id.btn_albumListActivity);
        pictureListActivity = (Button)findViewById(R.id.btn_pictureListActivity);
        createAlbumActivity = (Button)findViewById(R.id.btn_createAlbumActivity);
        pictureActivity = (Button)findViewById(R.id.btn_pictureActivity);
        editAlbumActivity = (Button)findViewById(R.id.btn_editAlbumActivity);
        editPictureActivity = (Button)findViewById(R.id.btn_editPictureActivity);
        btnSignout = (Button)findViewById(R.id.btn_signOut);

        // - onClickListener
        logInActivity.setOnClickListener(this);
        albumListActivity.setOnClickListener(this);
        pictureListActivity.setOnClickListener(this);
        createAlbumActivity.setOnClickListener(this);
        pictureActivity.setOnClickListener(this);
        editAlbumActivity.setOnClickListener(this);
        editPictureActivity.setOnClickListener(this);
        btnSignout.setOnClickListener(this);
    }

    private void startAuthStateListener() {
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    Log.i(TAG, "User is signed in as: " + firebaseAuth.getCurrentUser().getEmail());
                    btnSignout.setVisibility(View.VISIBLE);
                } else {
                    Log.i(TAG, "User is NOT signed in");
                    btnSignout.setVisibility(View.GONE);
                }
            }
        };
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.btn_albumListActivity:
                intent = new Intent(this, AlbumListActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_loginActivity:
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_pictureListActivity:
                intent = new Intent(this, PictureListActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_createAlbumActivity:
                intent = new Intent(this, CreateAlbumActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_pictureActivity:
                intent = new Intent(this,PictureActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_editAlbumActivity:
                intent = new Intent(this,EditAlbumActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_editPictureActivity:
                intent = new Intent(this,EditPictureActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_signOut:
                firebaseAuth.signOut();
                startLogInActivity();
                break;
        }
    }

    private void startLogInActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
