package com.example.matthewtaila.memoirs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.matthewtaila.memoirs.Constants.Constantss;
import com.example.matthewtaila.memoirs.Model.Picture;

public class PictureActivity extends AppCompatActivity {

    // - UI
    ImageView iv_pictureFull;

    // - Variables
    Picture picture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture);
        referenceViews();
        picture = getIntent().getParcelableExtra(Constantss.PASS_PICTURE);
        onLoadImage(picture, iv_pictureFull);

    }

    private void onLoadImage(Picture picture, ImageView iv_pictureFull) {
        Glide.with(this).load(picture.getImageUrl()).into(iv_pictureFull);
    }

    private void referenceViews() {
        iv_pictureFull = (ImageView)findViewById(R.id.iv_pictureFull);
    }
}
