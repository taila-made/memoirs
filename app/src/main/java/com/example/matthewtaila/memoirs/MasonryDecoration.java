package com.example.matthewtaila.memoirs;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by matthewtaila on 10/24/16.
 */

public class MasonryDecoration extends RecyclerView.ItemDecoration {
    private final int mSpace;

    public MasonryDecoration(int mSpace) {
        this.mSpace = mSpace;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = mSpace;
        outRect.right = mSpace;
        outRect.bottom = mSpace;

        if (parent.getChildAdapterPosition(view)==0){
            outRect.top = mSpace;
        }
    }
}
