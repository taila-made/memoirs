package com.example.matthewtaila.memoirs;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.matthewtaila.memoirs.Constants.Constantss;
import com.example.matthewtaila.memoirs.Model.Album;
import com.example.matthewtaila.memoirs.Model.Picture;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class AddImageActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "AddImageActivity";

    // - UI
    ImageButton selectImage;
    EditText selectTitle;
    EditText selectDescription;
    Button storeImage;
    private Bitmap imageBitmap;

    // - Logic
    public final static int PICK_PHOTO_CODE = 1046;
    public final static int SELECT_MOOD = 10;
    public final static String PASS_PICTURE = "picture";
    Album album = new Album();

    Picture picture;

    // - Firebase.Authentication
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authListener;
    // - Firebase Storage
    StorageReference storageRef;
    // - Firebase RealtimeDatabase
    DatabaseReference databaseRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_image_activty);
        album = getIntent().getParcelableExtra(Constantss.PASS_ALBUM);
        firebaseAuth = FirebaseAuth.getInstance();
        picture = new Picture();
        referenceViews();
        startAuthStateListener();
        onSelectImage();
    }


    private void startAuthStateListener() {
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    Log.i(TAG, "User is signed in as: " + firebaseAuth.getCurrentUser().getEmail());
                } else {Log.i(TAG, "User is NOT signed in");}
            }
        };
    }


    private void onSelectImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (intent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(intent, PICK_PHOTO_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == PICK_PHOTO_CODE) {
            if (data != null) {
                Uri selectedImage = data.getData();
                try {
                    imageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    selectImage.setImageBitmap(imageBitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == SELECT_MOOD){
            picture = data.getParcelableExtra(PASS_PICTURE);
            Toast.makeText(this, "ASDASDASDASD", Toast.LENGTH_LONG).show();
        }
    }

    private void referenceViews() {
        //  - Views
        selectImage = (ImageButton)findViewById(R.id.ibtn_selectPicture);
        selectTitle = (EditText)findViewById(R.id.et_selectTitle);
        selectDescription = (EditText)findViewById(R.id.et_selectDescription);
        storeImage = (Button)findViewById(R.id.btn_saveImage);
        // - onClickListener
        selectImage.setOnClickListener(this);
        storeImage.setOnClickListener(this);
        // - Stub Image
        // selectImage.setImageResource(R.drawable.test);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ibtn_selectPicture:
                // TODO - logic for choosing an image
                break;
            case R.id.btn_saveImage:
                onFirebaseStorage();
                break;
        }
    }

    private void onFirebaseStorage() {

        databaseRef = Constantss.FB_USER_PICTURE_REF.child(firebaseAuth.getCurrentUser().getUid()).child(album.getUid());
        final DatabaseReference pictureRef = databaseRef.push();

        // Firebase Storage - Store image FILE
        storageRef = FirebaseStorage.getInstance().getReference()
                .child("pictures")
                .child(firebaseAuth.getCurrentUser().getUid())
                .child(album.getUid())
                .child(pictureRef.getKey());

        selectImage.setDrawingCacheEnabled(true);
        selectImage.buildDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        // Start upload task
        UploadTask uploadTask = storageRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("FAILED", "FAILED");
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUri = taskSnapshot.getDownloadUrl();
                // - Stores Picture object to Firebase Realtime Database
                String pictureTitle = selectTitle.getText().toString();
                String pictureDescription = selectDescription.getText().toString();
                picture.setPictureTitle(pictureTitle);
                picture.setPictureDescription(pictureDescription);
                picture.setImageUrl(taskSnapshot.getDownloadUrl().toString());
                picture.setUid(pictureRef.getKey());
                pictureRef.setValue(picture);
                finish();
            }
        });
    }
}
