package com.example.matthewtaila.memoirs.Constants;

import android.animation.Animator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.WindowManager;
import android.animation.AnimatorListenerAdapter;
import android.view.animation.Animation;

/**
 * Created by matthewtaila on 10/29/16.
 */

public class Utils {


    private static int screenWidth = 0;
    private static int screenHeight = 0;

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int getScreenHeight(Context c) {
        if (screenHeight == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenHeight = size.y;
        }

        return screenHeight;
    }

    public static int getScreenWidth(Context c) {
        if (screenWidth == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
        }

        return screenWidth;
    }

    public static boolean isAndroid5() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static Animator circleReveal(View view) {
        int cx = view.getMeasuredWidth() /2;
        int cy = view.getMeasuredHeight() /2;

        int finalRadius = Math.max(view.getWidth(), view.getHeight()) / 2;

        Animator anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);

        view.setVisibility(View.VISIBLE);
        return anim;
    }

    public static Animator circleReveal(View view, int centerX, int centerY) {
        int finalRadius = Math.max(view.getWidth(), view.getHeight()) / 2;

        Animator anim = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, 0, finalRadius);

        view.setVisibility(View.VISIBLE);
        return anim;
    }

    public static Animator circleReveal(View view, int centerX, int centerY, int finalRadius) {
        Animator anim = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, 0, finalRadius);

        view.setVisibility(View.VISIBLE);
        return anim;
    }

    public static Animator circleReveal(View startingView, View finalView) {
        int cx = startingView.getMeasuredWidth() /2;
        int cy = startingView.getMeasuredHeight() /2;
        int finalRadius = Math.max(finalView.getWidth(), finalView.getHeight());
        Animator anim = ViewAnimationUtils.createCircularReveal(startingView, cx, cy, 0, finalRadius);
        finalView.setVisibility(View.VISIBLE);
        return anim;
    }

}
