package com.example.matthewtaila.memoirs.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by matthewtaila on 9/18/16.
 */
public class Album implements Parcelable {

    String title;
    String date;
    String uid;
    String albumProfilePicURL;

    public Album() {
    }

    protected Album(Parcel in) {
        title = in.readString();
        date = in.readString();
        uid = in.readString();
        albumProfilePicURL = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(date);
        dest.writeString(uid);
        dest.writeString(albumProfilePicURL);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Album> CREATOR = new Creator<Album>() {
        @Override
        public Album createFromParcel(Parcel in) {
            return new Album(in);
        }

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }
    };

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAlbumProfilePicURL() {
        return albumProfilePicURL;
    }

    public void setAlbumProfilePicURL(String albumProfilePicURL) {
        this.albumProfilePicURL = albumProfilePicURL;
    }
}
