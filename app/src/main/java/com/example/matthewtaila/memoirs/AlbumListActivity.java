package com.example.matthewtaila.memoirs;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Transition;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.matthewtaila.memoirs.Constants.Constantss;
import com.example.matthewtaila.memoirs.Model.Album;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class AlbumListActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "AlbumListActivity";

    // - Logic
    private ArrayList<Album> albumArrayList;
    private AlbumAdapter albumAdapter;
    private Transition.TransitionListener enterTransitionListener;

    // - UI
    private RecyclerView recyclerView;
    private FloatingActionButton createAlbumButton;
    public Button signOutButton;

    // - Firebase Realtime Database
    private DatabaseReference albumListRef;
    // - Firebase.Authentication
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authListener;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_list);
        referenceViews();
        onEnterTransition();
        albumArrayList = new ArrayList<>(); // instantiate ArrayList<Album>
        firebaseAuth = FirebaseAuth.getInstance();
        startAuthStateListener();
        setupFirebaseAlbumRef(); // Reference FireBase path to user's albums
        albumListFirebasePull(); // Sets ChildEventListener on user's album FireBase path
        setupAlbumRecyclerView(); // RecyclerView logic
    }

    private void onEnterTransition() {
        enterTransitionListener = new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(Transition transition) {
                getWindow().getEnterTransition().removeListener(enterTransitionListener);
            }

            @Override
            public void onTransitionEnd(Transition transition) {

            }

            @Override
            public void onTransitionCancel(Transition transition) {

            }

            @Override
            public void onTransitionPause(Transition transition) {

            }

            @Override
            public void onTransitionResume(Transition transition) {

            }
        };
        getWindow().getEnterTransition().addListener(enterTransitionListener);
    }

    private void referenceViews() {
        // - Views
        createAlbumButton = (FloatingActionButton) findViewById(R.id.btn_createAlbum);
        recyclerView = (RecyclerView)findViewById(R.id.rv_albumList);
        signOutButton = (Button)findViewById(R.id.btn_signOutUser);
        // - onClickListener
        createAlbumButton.setOnClickListener(this);
        signOutButton.setOnClickListener(this);
    }

    private void startAuthStateListener() {
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    Log.i(TAG, "User is signed in as: " + firebaseAuth.getCurrentUser().getEmail());
                } else {
                    Log.i(TAG, "User is NOT signed in");
                }
            }
        };
    }

    private void setupFirebaseAlbumRef() {
        albumListRef = Constantss.FB_USER_ALBUM_REF.child(firebaseAuth.getCurrentUser().getUid());
    }

    private void albumListFirebasePull() {
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override // Each album object present in the Firebase path is added to ArrayList<Album>
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                albumArrayList.add(dataSnapshot.getValue(Album.class));
                albumAdapter.notifyItemInserted(albumArrayList.size()-1);
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        // Set ChildEventListener on Firebase path for user's album
        albumListRef.addChildEventListener(childEventListener);
    }

    private void setupAlbumRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        albumAdapter = new AlbumAdapter(albumArrayList, this);
        recyclerView.setAdapter(albumAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_createAlbum: // Logic for activity change to CreateAlbumActivity
                Intent intent = new Intent(this, CreateAlbumActivity.class);
                intent.putExtra("centerX", createAlbumButton.getLeft()+createAlbumButton.getMeasuredWidth()/2);
                intent.putExtra("centerY", createAlbumButton.getTop()+createAlbumButton.getMeasuredWidth()/2);
                startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle());
                break;
            case R.id.btn_signOutUser:
                firebaseAuth.signOut();
                startLogInActivity();
                supportFinishAfterTransition();
        }
    }

    private void startLogInActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
