package com.example.matthewtaila.memoirs;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.matthewtaila.memoirs.Constants.Constantss;
import com.example.matthewtaila.memoirs.Model.Album;
import com.example.matthewtaila.memoirs.Model.Picture;

import java.util.ArrayList;

/**
 * Created by matthewtaila on 9/29/16.
 */

public class PictureAdapter extends RecyclerView.Adapter<PictureAdapter.ViewHolder> {

    private ArrayList<Picture> pictureArrayList;
    private Context context;

    public PictureAdapter(ArrayList<Picture> pictureArray, Context context) {
        this.pictureArrayList = pictureArray;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View pictureVIew = layoutInflater.inflate(R.layout.item_picture_list_cardview, parent, false);
        PictureAdapter.ViewHolder viewHolder = new PictureAdapter.ViewHolder(pictureVIew);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Picture picture = pictureArrayList.get(position);
        ImageView pictureCover = holder.ivPicture;
        setPicture(picture, pictureCover);
        pictureCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PictureActivity.class);
                intent.putExtra(Constantss.PASS_PICTURE, picture);
                context.startActivity(intent);
            }
        });
    }

    private void setPicture(Picture picture, ImageView pictureCover) {
        Glide.with(context).load(picture.getImageUrl()).override(300, 300).fitCenter().into(pictureCover);
    }

    @Override
    public int getItemCount() {
        return pictureArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView ivPicture;
        public ViewHolder(View itemView) {
            super(itemView);
            ivPicture = (ImageView)itemView.findViewById(R.id.ibtn_pictureCover);
        }
    }

}
