package com.example.matthewtaila.memoirs;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.transition.Transition;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.matthewtaila.memoirs.Constants.Constantss;
import com.example.matthewtaila.memoirs.Constants.Utils;
import com.example.matthewtaila.memoirs.Model.Album;
import com.example.matthewtaila.memoirs.Model.Picture;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class PictureListActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "PictureListActivity";

    // - Logic
    private Album album;
    private ArrayList<Picture> pictureArrayList;
    private PictureAdapter pictureListAdapter;
    private Transition.TransitionListener enterTransitionListener;


    // - UI
    RelativeLayout rl_pictureListContainer;
    RelativeLayout rl_albumProfileContainer;
    RelativeLayout rl_topLayer;
    RelativeLayout rl_bottomLayer;
    ImageView iv_albumProfileImage;
    ImageButton ibtn_editAlbum;
    View whiteBackGround;
    TextView tv_albumProfileTitle;
    private RecyclerView recyclerView;
    private ImageButton pictureCover;
    private FloatingActionButton addImageFAB;

    // - Firebase Storage
    private StorageReference storageRef;
    // - Firebase Realtime Database
    private DatabaseReference pictureListRef;
    // - Firebase.Authentication
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_list);
        album = getIntent().getParcelableExtra(Constantss.FB_ALBUM);
        referenceViews();
        onEnterTransition();
        pictureArrayList = new ArrayList<>();
        fireBaseUser();
        setFBRealtimeDatabaseRef();
        pictureListPull();
        setUpRecyclerView();

    }

    private void onEnterTransition() {
        enterTransitionListener = new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(Transition transition) {

                Log.d(TAG, "onTransitionEnd: ");
                AnimatorListenerAdapter animatorListenerAdapter = new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationCancel(Animator animation) {
                        super.onAnimationCancel(animation);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        getWindow().getEnterTransition().removeListener(enterTransitionListener);
                        animateViews();
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }
                };
                int centerX = getIntent().getIntExtra("centerX", 0);
                int centerY = getIntent().getIntExtra("centerY", 0);
                Animator animator = Utils.circleReveal(rl_pictureListContainer, centerX, centerY, Math.max(rl_pictureListContainer.getMeasuredHeight(), rl_pictureListContainer.getMeasuredWidth()));
                animator.addListener(animatorListenerAdapter);
                animator.start();
            }

            @Override
            public void onTransitionEnd(Transition transition) {

            }

            @Override
            public void onTransitionCancel(Transition transition) {

            }

            @Override
            public void onTransitionPause(Transition transition) {

            }

            @Override
            public void onTransitionResume(Transition transition) {

            }
        };
        getWindow().getEnterTransition().addListener(enterTransitionListener);
    }

    private void animateViews() {

        rl_topLayer.animate()
                .translationY(0)
                .setDuration(300)
                .setStartDelay(100)
                .setInterpolator(new DecelerateInterpolator(1));

        rl_bottomLayer.animate()
                .translationY(0)
                .setDuration(300)
                .setStartDelay(250)
                .setInterpolator(new DecelerateInterpolator(1));

        tv_albumProfileTitle.animate()
                .translationY(0)
                .setDuration(300)
                .setStartDelay(250)
                .setInterpolator(new DecelerateInterpolator(1));

        recyclerView.animate()
                .translationY(0)
                .setDuration(300)
                .setStartDelay(550)
                .setInterpolator(new DecelerateInterpolator(1));

        whiteBackGround.animate()
                .alpha(1)
                .setDuration(300)
                .setStartDelay(550);

        ibtn_editAlbum.animate()
                .translationY(0)
                .setDuration(300)
                .setStartDelay(650)
                .setInterpolator(new DecelerateInterpolator());

    }

    private void pictureListPull() {
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                pictureArrayList.add(dataSnapshot.getValue(Picture.class));
                pictureListAdapter.notifyItemInserted(pictureArrayList.size()-1);
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        pictureListRef.addChildEventListener(childEventListener);
    }

    private void setFBRealtimeDatabaseRef() {
        pictureListRef = Constantss.FB_USER_PICTURE_REF.child(firebaseAuth.getCurrentUser().getUid()).child(album.getUid());
    }

    private void fireBaseUser() {
        firebaseAuth = FirebaseAuth.getInstance();
        startAuthStateListener();
    }

    private void startAuthStateListener() {
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    Log.i(TAG, "User is signed in as: " + firebaseAuth.getCurrentUser().getEmail());
                } else {
                    Log.i(TAG, "User is NOT signed in");
                }
            }
        };
    }

    private void setUpRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        pictureListAdapter = new PictureAdapter(pictureArrayList, this);
        recyclerView.setAdapter(pictureListAdapter);
    }

    private void referenceViews() {

        // - Reference Views
        rl_albumProfileContainer = (RelativeLayout)findViewById(R.id.rl_albumProfileContainer);
        rl_topLayer = (RelativeLayout)findViewById(R.id.rl_toplayer);
        rl_bottomLayer = (RelativeLayout)findViewById(R.id.rl_bottomLayer);
        whiteBackGround = findViewById(R.id.whitebackground);
        whiteBackGround.setAlpha(0f);
        iv_albumProfileImage = (ImageView)findViewById(R.id.profileImage);
        Glide.with(this).load(album.getAlbumProfilePicURL()).bitmapTransform(new CropCircleTransformation(this)).into(iv_albumProfileImage);
        tv_albumProfileTitle = (TextView)findViewById(R.id.tv_profile_album_title);
        tv_albumProfileTitle.setText(album.getTitle());
        pictureCover = (ImageButton)findViewById(R.id.ibtn_pictureCover);
        recyclerView = (RecyclerView)findViewById(R.id.pictureRecyclerView);
        rl_pictureListContainer = (RelativeLayout)findViewById(R.id.rl_pictureListContainer);
        recyclerView.setHasFixedSize(true);
        addImageFAB = (FloatingActionButton)findViewById(R.id.addImage);
        ibtn_editAlbum = (ImageButton) findViewById(R.id.ibtn_editAlbum);

        // - set views off Screen
        rl_topLayer.setTranslationY(-Utils.getScreenHeight(this));
        rl_bottomLayer.setTranslationY(-Utils.getScreenHeight(this));
        tv_albumProfileTitle.setTranslationY(-Utils.getScreenHeight(this));
        recyclerView.setTranslationY(Utils.getScreenHeight(this));
        ibtn_editAlbum.setTranslationY(-Utils.getScreenHeight(this));

        // set onClickListener
        addImageFAB.setOnClickListener(this);
        ibtn_editAlbum.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.addImage:
                Intent intent = new Intent(this, AddImageActivity.class);
                intent.putExtra(Constantss.PASS_ALBUM, album);
                startActivity(intent);
            case  R.id.ibtn_editAlbum:
                Toast.makeText(this, "HAVE TO ADD EDIT ALBUM FEATURE", Toast.LENGTH_LONG).show();
        }
    }
}
