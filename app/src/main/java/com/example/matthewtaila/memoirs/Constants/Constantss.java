package com.example.matthewtaila.memoirs.Constants;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

/**
 * Created by matthewtaila on 9/21/16.
 */

public class Constantss {

    // - String Constants
    public static final String FB_ALBUM = "Album";
    public static final String FB_PICTURE = "Images";
    public static final String PASS_ALBUM = "Album pass";
    public static final String PASS_PICTURE = "Picture pass";

    // - Mood Constants
    public static final String MOOD_EMPTY = "empty";
    public static final String MOOD_HAPPY = "Happy";
    public static final String MOOD_NEUTRAL = "Neutral";
    public static final String MOOD_SILLY = "Silly";
    public static final String MOOD_VHAPPY = "Very Happy";
    public static final String MOOD_SAD = "Sad";
    public static final String MOOD_NAUGHTY = "Horny";
    public static final String MOOD_CHEEKY = "Cheeky";
    public static final String MOOD_COOL = "Cool";

    // - FireBase Realtime DataBase
    public static final DatabaseReference FB_MEMOIRS_REFERENCE = FirebaseDatabase.getInstance().getReference();
    public static final DatabaseReference FB_USER_ALBUM_REF = FB_MEMOIRS_REFERENCE.child(Constantss.FB_ALBUM);
    public static final DatabaseReference FB_USER_PICTURE_REF = FB_MEMOIRS_REFERENCE.child(Constantss.FB_PICTURE);

    // - FireBase Storage
    public static final FirebaseStorage storage = FirebaseStorage.getInstance();
}
