package com.example.matthewtaila.memoirs;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.matthewtaila.memoirs.Constants.Constantss;
import com.example.matthewtaila.memoirs.Model.Album;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

import static com.example.matthewtaila.memoirs.R.id.profileImage;

/**
 * Created by matthewtaila on 9/18/16.
 */
public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {

    private ArrayList<Album> albumArrayList;
    private Context context;

    public AlbumAdapter(ArrayList<Album> albumArrayList, Context context) {
        this.albumArrayList = albumArrayList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View albumView = layoutInflater.inflate(R.layout.item_album_list_cardview, parent, false);
        ViewHolder viewHolder = new ViewHolder(albumView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Album album = albumArrayList.get(position);
        final View border = holder.borderView;
        final RelativeLayout relativeLayout = holder.relativeLayout; // set onClickListener
        TextView titleTextView = holder.tvAlbumTitle; // set album title
        final ImageView iv_albumCover = holder.iv_albumCover; // set album cover picture
        titleTextView.setText(album.getTitle());
        setAlbumImage(album, iv_albumCover);

        // Set onClickListener on each album cardView represented in the RecyclerView (Activity change to PictureListActivity)
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                border.animate().alpha(0f).setDuration(300).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        Intent intent = new Intent(context, PictureListActivity.class);
                        intent.putExtra("centerX", relativeLayout.getLeft()+relativeLayout.getMeasuredWidth()/2);
                        intent.putExtra("centerY", relativeLayout.getLeft()+relativeLayout.getMeasuredWidth()/2);
                        intent.putExtra(Constantss.FB_ALBUM, album);
                        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context, iv_albumCover, "albumProfile");
                        context.startActivity(intent, optionsCompat.toBundle());
                    }
                });


            }
        });
    }

    private void setAlbumImage(Album album, ImageView iv_albumCover) {
        Glide.with(context).load(album.getAlbumProfilePicURL()).bitmapTransform(new CropCircleTransformation(context)).into(iv_albumCover);
    }


    @Override
    public int getItemCount() {
        return albumArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public RelativeLayout relativeLayout;
        public TextView tvAlbumTitle;
        public ImageView iv_albumCover;
        public View borderView;
        public ViewHolder(View itemView) {
            super(itemView);
            iv_albumCover = (ImageView)itemView.findViewById(R.id.iv_albumCover);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.cv_albumContainer);
            tvAlbumTitle = (TextView)itemView.findViewById(R.id.tv_albumTitle);
            borderView = itemView.findViewById(R.id.imageBorder);
        }
    }

}
