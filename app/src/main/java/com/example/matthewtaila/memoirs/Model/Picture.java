package com.example.matthewtaila.memoirs.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.matthewtaila.memoirs.Constants.Constantss;

/**
 * Created by matthewtaila on 9/29/16.
 */

public class Picture implements Parcelable{

    String pictureTitle;
    String pictureDescription;
    String uid;
    String imageUrl;

    public Picture() {
    }

    protected Picture(Parcel in) {
        pictureTitle = in.readString();
        pictureDescription = in.readString();
        uid = in.readString();
        imageUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pictureTitle);
        dest.writeString(pictureDescription);
        dest.writeString(uid);
        dest.writeString(imageUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Picture> CREATOR = new Creator<Picture>() {
        @Override
        public Picture createFromParcel(Parcel in) {
            return new Picture(in);
        }

        @Override
        public Picture[] newArray(int size) {
            return new Picture[size];
        }
    };

    public String getPictureTitle() {
        return pictureTitle;
    }

    public void setPictureTitle(String pictureTitle) {
        this.pictureTitle = pictureTitle;
    }

    public String getPictureDescription() {
        return pictureDescription;
    }

    public void setPictureDescription(String pictureDescription) {
        this.pictureDescription = pictureDescription;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
